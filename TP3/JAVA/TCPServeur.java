import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;


public class TCPServeur {

	public static void main(String[] args)
	{
		ServerSocket socketServeur;
		String addresseDest;
		int port;

    if(args.length == 1)
    {
        port =  Integer.parseInt(args[0]);

        try
				{
					socketServeur = new ServerSocket(port);
					System.out.println("Lancement du serveur.");
					System.out.println("En attente d'un client.");
					Socket socketClient = socketServeur.accept();
					String message = "";
					System.out.println("Connexion client.");
					BufferedReader in = new BufferedReader(new InputStreamReader(socketClient.getInputStream()));
					PrintStream out = new PrintStream(socketClient.getOutputStream());

					while(true)
					{

						message = in.readLine();

						if(message == null)
						{
							System.out.println("Déconnexion du client.");
							in.close();
							out.close();
							socketClient.close();
							break;
						}
						else
						{
							System.out.println(">"+message);
							//out.println("> "+ message);
							//out.flush();
						}
					}

					socketServeur.close();

        } catch (IOException e)
				{
          e.printStackTrace();
          System.exit(0);
        }
    }
    else if(args.length > 1)
    {
      System.out.println("Trop d'arguments");
      System.exit(1);
    }
    else
    {
      System.out.println("Pas assez d'arguments");
      System.exit(1);
    }
  }

}
