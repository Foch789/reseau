#!/bin/bash

#Debut programme
echo "Bonjour, voici un petit script qui permettra de tester mon code."
echo "Avant de commencer il me faudrait que vous remplissez une variable qu'on utilisera pour l'argument de mes programmes."
read -p "Le port : " port 


javac JAVA/TCPClient.java
javac JAVA/TCPServeur.java

java_client="java TCPClient localhost $port"
java_serveur="java TCPServeur $port"

python_client="python3 ../PYTHON/main.py localhost $port"

realisation_test() {

    local message_envoyer=$'salut\n
    '
    local message_ress=$'Lancement du serveur.\nEn attente d\'un client.\nConnexion client.\n>salut\nDéconnexion du client.'
    
    echo "Debut du test entre $1 et $2"
    echo 
    
    res="$(mktemp)"
    ${!server} > $res &
    
    sleep 0.5
    
    ${!client} <<< "$(echo -en "$message_envoyer")" > /dev/null
    
    sleep 0.5
    
    if [ "$(echo -en "$message_ress")" == "$(cat $res)" ]
    then
        echo "ok"
        echo
    else
        echo "error"
        echo "server log:"
        cat $res
        echo
    fi


    rm $res
    
    echo $'Fin du test\n'

}

clear() {

rm JAVA/TCPClient.class
rm JAVA/TCPServeur.class

}


cd JAVA

client=java_client; server=java_serveur; realisation_test "Java_client" "Java_serveur"
client=python_client; server=java_serveur; realisation_test "Python_client" "Java_serveur"

#read -p "Presser une touche pour continuer... "

#clear
