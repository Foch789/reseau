import socket
import sys


def envoyer(message):
    socket.send(message.encode())

if len(sys.argv) != 3:
    print("Erreur il faut 2 arguments")
    sys.exit(1)

adresseDest = sys.argv[1]
port = int(sys.argv[2])

try:
    socket = socket.create_connection((adresseDest, port))
except IOError:
    print("Erreur")
    sys.exit(1)


while(True):
    msg = input('Entrez un message : ')
    if msg != "":
        msg += "\n"

        envoyer(msg)
    else:
        break

print("Exit client.")
socket.close()
