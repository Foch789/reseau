import java.io.*;
import java.net.*;
import java.util.concurrent.TimeUnit;
import java.util.*;

public class Stress1
{

  public static void main(String[] args)
  {
    int nombreClient;
    PrintWriter out;
    int port;

    if(args.length == 2)
    {
      port = Integer.parseInt(args[0]);
      nombreClient = Integer.parseInt(args[1]);
    }
    else
    {
      System.out.println("Usage: java Stress1 (port) (number clients)");
      return;
    }

    List<String[]> test = new ArrayList<String[]>(nombreClient);

    try
    {
      System.nanoTime();
      //Socket [] echoSocketTab = new Socket [nombreClient];
      for (int i = 0; i < nombreClient ;i++ )
      {
          Socket echoSocket = new Socket("localhost", port);
          double temps = System.nanoTime();
          out = new PrintWriter(echoSocket.getOutputStream(), true);
          out.println("client stress1 n° "+(i+1));
          echoSocket.close();
          temps = (System.nanoTime() - temps)/ 1000000000.0;
          String [] ligne = new String[2];
          ligne[0] = "client stress1 n° "+(i+1);
          ligne[1] = ""+temps;
          test.add(i,ligne);
      }

      //TimeUnit.SECONDS.sleep(10);
      double temps = System.nanoTime();
      String [] ligne = new String[2];
      ligne[0] = "Temps total ";
      ligne[1] = ""+temps+ " nanotime";
      test.add(ligne);

      /*for (int i = 0; i < nombreClient ;i++ )
      {
          echoSocketTab[i].close();
      }*/

      //TimeUnit.SECONDS.sleep(1);

      ecrireCSV(test);

    } catch (Exception e)
    {
      e.printStackTrace();
    }
    return;
  }

  public static void ecrireCSV(List<String[]> test)
  {
    try (FileWriter writer = new FileWriter("test.csv")){
        for (String[] strings : test) {
            for (int i = 0; i < strings.length; i++) {
                writer.append(strings[i]);
                if(i < (strings.length-1))
                    writer.append(';');
            }
            writer.append(System.lineSeparator());
        }
        writer.flush();
    } catch (IOException e) {
        e.printStackTrace();
    }
  }

}
