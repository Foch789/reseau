import socket
import sys

def Att_message():
    msg = raw_input('Entrez un message (STOP pour arreter le client) : ')
    if msg != "STOP":
        Envoyer(msg)
    else :
        Close()


def Envoyer(message):
    socket.sendto(message, (adresseDest, port))
    Att_message()

def Close():
    message = "Deconnexion du client."
    socket.sendto(message, (adresseDest, port))
    print(message)
    socket.close()

if len(sys.argv) != 3:
    print("Erreur il faut 2 arguments")
    exit(1)

adresseDest = sys.argv[1]
port = int(sys.argv[2])

socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
Att_message()
