package src.main;

public class Reseau {

	public static void main(String[] args)
	{

		if(args.length == 3)
		{
			String adresse = args[1];
			int port =  Integer.parseInt(args[2]);

			if( args[0].equals("serveur"))
			{
				System.out.println("Programme : " +args[0]);
				ServeurUDP serveur = new ServeurUDP(adresse,port);
				serveur.recevoir();

			}
			else if (args[0].equals("client"))
			{
				System.out.println("Programme : " +args[0]);
				UDPClient client = new UDPClient(adresse,port);
				client.Pret();
			}
			else
			{
				System.out.println("Erreur le premier argument c'est soit serveur soit client.");
				System.exit(1);
			}


		}
		else if(args.length > 3)
		{
			System.out.println("Trop d'arguments");
			System.exit(1);
		}
		else
		{
			System.out.println("Pas assez d'arguments");
			System.exit(1);
		}


	}
}
