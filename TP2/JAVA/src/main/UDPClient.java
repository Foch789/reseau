package src.main;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

public class UDPClient {
	
	public static DatagramSocket socket;
	public String addresseDest;
	public int portDest;
	boolean connecté;
	
	public UDPClient(String addresseDest,int portDest)
	{
		this.addresseDest = addresseDest;
		this.portDest = portDest;
		
		try
		{
			socket = new DatagramSocket();
			connecté = true;
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
	}
	
	public void Pret()
	{
		connexion();
	}
	
	private void connexion()
	{
		System.out.println("Connexion au client addresse "+addresseDest+" et port "+portDest+".");
		preparePaquet("Connexion de Lolo\n");
	}
	
	@SuppressWarnings("resource")
	private void Lire()
	{
		Scanner clavier = new Scanner(System.in);
		String message = null;
		
		System.out.println("Message à envoyer au serveur : ");
		message = clavier.nextLine();
		
		if(message.isEmpty())
		{
			connecté = false;
			close();
		}
		else
		{
			preparePaquet(message+="\n");
		}	
	}
	
	private void preparePaquet(String message)
	{	
		byte[] data = new byte[1024];
		
		InetAddress adresse = null;
		try {
			adresse = InetAddress.getByName(addresseDest);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		data = message.getBytes();
		DatagramPacket paquet = new DatagramPacket(data, data.length,adresse,portDest);
		
		envoyer(paquet);
	}

	private void envoyer(DatagramPacket paquet)
	{
		try {
			socket.send(paquet);
		} catch (IOException e) {
			e.printStackTrace();
		}	
		
		if(connecté)
			Lire();
	}
	
	private void close()
	{	
		System.out.println("Déconnexion.");
		preparePaquet("Déconnexion de Lolo\n");
		socket.close();
	}

}
