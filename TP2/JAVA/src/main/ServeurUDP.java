package src.main;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class ServeurUDP {

	public static DatagramSocket socket;
	public String addresseDest;
	public int portDest;


	public ServeurUDP(String addresseDest,int portDest)
	{
		this.addresseDest = addresseDest;
		this.portDest = portDest;

		try
		{
			socket = new DatagramSocket(portDest);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	public void recevoir()
	{
		while(true)
		{

			byte [] data = new byte[1024];
			DatagramPacket paquet = new DatagramPacket(data,data.length);
			try {
				System.out.println("En attente d'un paquet");
				socket.receive(paquet);
				affiche(paquet);
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}

		}

		socket.close();
	}

	public void affiche(DatagramPacket paquet)
	{
		String message = new String(paquet.getData());
		System.out.println("> " + message);
		envoyer(paquet,message);
	}

	public void envoyer(DatagramPacket paquet, String message)
	{
		byte [] data = new String("Réponse du serveur : "+ message).getBytes();
		DatagramPacket paquetEnvoyer = new DatagramPacket(data, data.length,paquet.getAddress(),paquet.getPort());
		try {
			socket.send(paquetEnvoyer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


}
