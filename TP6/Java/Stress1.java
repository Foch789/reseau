import java.io.*;
import java.net.*;
import java.util.concurrent.TimeUnit;
import java.util.*;
import java.nio.channels.SocketChannel;
import java.nio.ByteBuffer;

public class Stress1
{

  public static void main(String[] args)
  {
    int nombreClient;
    int port;
    ByteBuffer buffer;

    if(args.length == 2)
    {
      port = Integer.parseInt(args[0]);
      nombreClient = Integer.parseInt(args[1]);
    }
    else
    {
      System.out.println("Usage: java Stress1 (port) (number clients)");
      return;
    }

    List<String[]> test = new ArrayList<String[]>(nombreClient);

    try
    {
      double start = System.nanoTime();
      for (int i = 0; i < nombreClient ;i++ )
      {

          double temps = System.nanoTime();

          SocketChannel socketChannel = SocketChannel.open();
          socketChannel.connect(new InetSocketAddress("localhost", port));

          buffer = ByteBuffer.allocate(40);
          buffer.clear();
          String texte = "client stress1 n "+(i+1);
          buffer.put(texte.getBytes());
          //socketChannel.write(buffer);
          buffer.flip();

          while(buffer.hasRemaining()) {
            socketChannel.write(buffer);
          }

          temps = (System.nanoTime() - temps)/ 1000000000.0;
          String [] ligne = new String[2];
          ligne[0] = "client stress1 n° "+(i+1);
          ligne[1] = ""+temps;
          test.add(i,ligne);
          buffer.clear();
      }
      double end = System.nanoTime();
      double temps  = ((end - start) / 1000000000.0);
      String [] ligne = new String[2];
      ligne[0] = "Temps total ";
      ligne[1] = ""+temps+ " secondes";
      test.add(ligne);
      ecrireCSV(test);

    } catch (Exception e)
    {
      e.printStackTrace();
    }
    return;
  }

  public static void ecrireCSV(List<String[]> test)
  {
    try (FileWriter writer = new FileWriter("test.csv")){
        for (String[] strings : test) {
            for (int i = 0; i < strings.length; i++) {
                writer.append(strings[i]);
                if(i < (strings.length-1))
                    writer.append(';');
            }
            writer.append(System.lineSeparator());
        }
        writer.flush();
    } catch (IOException e) {
        e.printStackTrace();
    }
  }

}
