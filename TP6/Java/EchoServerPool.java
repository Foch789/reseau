/* echo / serveur basique
   Master Informatique 2012 -- Université Aix-Marseille
   Bilel Derbel, Emmanuel Godard
*/

import java.net.*;
import java.util.concurrent.*;
import java.util.*;
import java.io.*;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.SelectionKey;
import java.util.concurrent.Executors;
import java.util.Scanner;
import java.nio.ByteBuffer;

class EchoServerPool {

  /* Démarrage et délégation des connexions entrantes */
  public void demarrer(int port) {
    ServerSocketChannel ssocket; // socket d'écoute utilisée par le serveur
    //int compteur = 0;
    System.out.println("Lancement du serveur sur le port " + port);
    try {
      ssocket = ServerSocketChannel.open();
      ssocket.socket().bind(new InetSocketAddress(port));
      Selector select = Selector.open();
      ssocket.configureBlocking(false);
      ssocket.register(select,SelectionKey.OP_ACCEPT);
      ByteBuffer buffer;

      while(true)
      {
        int num = select.select();
        Iterator<SelectionKey> keys = select.selectedKeys().iterator();
        while(keys.hasNext()){
          SelectionKey sk = keys.next();
          keys.remove();
          if(sk.isAcceptable())
          {
            SocketChannel client = ssocket.accept();
            client.configureBlocking(false);
            client.register(select,SelectionKey.OP_READ);
          }
          if (sk.isReadable())
          {
            //System.out.println("READ");
            SocketChannel client = (SocketChannel) sk.channel();
            buffer = ByteBuffer.allocate(128);
            client.read(buffer);
            buffer.flip();
            int len = buffer.remaining();
            if(len > 0)
              System.out.println(new String(buffer.array()));
            buffer.clear();
          }
        }
      }
    }
    catch (IOException ex) {
      System.out.println("Arrêt anormal du serveur."+ex);
      return;
    }
  }


  public static void main(String[] args) {
    int argc = args.length;
    EchoServerPool serveur;

    /* Traitement des arguments */
    if (argc == 1) {
      try {
        serveur = new EchoServerPool();
        serveur.demarrer(Integer.parseInt(args[0]));

      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    else {
      System.out.println("Usage: java EchoServer port");
    }
    return;
  }

}
