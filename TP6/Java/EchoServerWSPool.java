/* echo / serveur basique
   Master Informatique 2012 -- Université Aix-Marseille
   Bilel Derbel, Emmanuel Godard
*/

import java.net.*;
import java.util.concurrent.*;
import java.util.*;
import java.io.*;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.SelectionKey;
import java.util.concurrent.Executors;
import java.util.Scanner;
import java.nio.ByteBuffer;

class EchoServerWSPool {

  /* Démarrage et délégation des connexions entrantes */
  public void demarrer(int port) {
    ServerSocketChannel ssocket; // socket d'écoute utilisée par le serveur

    //int compteur = 0;
    System.out.println("Lancement du serveur sur le port " + port);
    try {
      ssocket = ServerSocketChannel.open();
      ssocket.socket().bind(new InetSocketAddress(port));
      Selector select = Selector.open();
      ssocket.configureBlocking(false);
      ssocket.register(select,SelectionKey.OP_ACCEPT);
      ExecutorService exuc;
      System.out.println("Voleur");
      exuc = Executors.newWorkStealingPool();

      while(true)
      {
        int num = select.select();
        Set<SelectionKey>keySync = new HashSet<SelectionKey>(select.selectedKeys());
        Iterator<SelectionKey> key = keySync.iterator();
        while(key.hasNext()){
          SelectionKey sk = key.next();
          key.remove();
          if(!sk.isValid())
          {
            continue;
          }
          if(sk.isValid() && sk.isAcceptable())
          {
            Handler test = new Handler(ssocket.accept(),select);
            exuc.execute(test);
          }
        }
      }
    }
    catch (IOException ex) {
      System.out.println("Arrêt anormal du serveur."+ex);
      return;
    }
  }


  public static void main(String[] args) {
    int argc = args.length;
    EchoServerWSPool serveur;

    /* Traitement des arguments */
    if (argc == 1) {
      try {
        serveur = new EchoServerWSPool();
        serveur.demarrer(Integer.parseInt(args[0]));

      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    else {
      System.out.println("Usage: java EchoServer port");
    }
    return;
  }

  static class Handler implements Runnable {

    SocketChannel client;
    Selector select;

    static int threadNum = 0;

    Handler(SocketChannel socket,Selector select) throws IOException {
      this.select = select;
      client = socket;
      client.configureBlocking(false);
      client.register(select,SelectionKey.OP_READ);
      threadNum ++;
    }

    public void run() {

      System.out.println("Debut thread"+threadNum);

      try{
        while(true)
        {
          int num = select.select();
          Set<SelectionKey>keySync = new HashSet<SelectionKey>(select.selectedKeys());
          Iterator<SelectionKey> key = keySync.iterator();
          while(key.hasNext()){
              SelectionKey sk = key.next();
              key.remove();
              if (sk.isReadable())
              {
                send(sk);
              }
          }
        }
      }
    catch (Exception e) {
        e.printStackTrace();
      }
    }

    public void send( SelectionKey sk)
    {
      ByteBuffer buffer;
      try{
          if (sk.isValid() && sk.isReadable())
          {
            SocketChannel client = (SocketChannel) sk.channel();
            buffer = ByteBuffer.allocate(128);
            client.read(buffer);
            buffer.flip();
            int len = buffer.remaining();
            if(len > 0)
              System.out.println(new String(buffer.array()));
            buffer.clear();
          }
          else
          {
            sk.cancel();
          }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

}
