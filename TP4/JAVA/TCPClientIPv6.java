import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.InetAddress;
import java.util.Scanner;
import java.net.Inet6Address;


public class TCPClientIPv6 {

  public static void main(String[] args)
  {
    Socket socketClient;
  	String addresseDest;
  	int port;
  	PrintStream output;

    if(args.length == 2)
    {
        addresseDest = args[0];
        port =  Integer.parseInt(args[1]);

        try {

          byte add[] = new byte [16];
          Inet6Address ip1 = Inet6Address.getByAddress(addresseDest, add, 5);
          socketClient = new Socket(ip1,port);
          output = new PrintStream(socketClient.getOutputStream());
          System.out.println("Connexion au serveur addresse "+addresseDest+" et port "+port+".");

          Scanner clavier = new Scanner(System.in);
      		String message = null;

      		while(true)
      		{
      			System.out.println("Message à envoyer au serveur : ");
      			message = clavier.nextLine();

      			if(message.isEmpty())
              break;
      			else
      			{
      				output.println(message);
      				message = null;
      			}
      		}

          System.out.println("Déconnexion.");
      		socketClient.close();

        } catch (IOException e) {
          e.printStackTrace();
          System.exit(0);
        }
    }
    else if(args.length > 2)
    {
      System.out.println("Trop d'arguments");
      System.exit(1);
    }
    else
    {
      System.out.println("Pas assez d'arguments");
      System.exit(1);
    }
  }


}
